package vn.laptrinh.football.domain.model

data class Leagues(
    val leagues: List<League> = emptyList()
)
