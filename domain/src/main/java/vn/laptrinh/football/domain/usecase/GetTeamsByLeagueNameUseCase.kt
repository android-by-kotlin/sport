package vn.laptrinh.football.domain.usecase

import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus
import vn.laptrinh.football.domain.model.Teams
import vn.laptrinh.football.domain.repository.TeamsRepository
import javax.inject.Inject

class GetTeamsByLeagueNameUseCase @Inject constructor(
    private val teamsRepository: TeamsRepository
) {

    suspend operator fun invoke(
        leagueName: String
    ): NetworkResponse<Teams> {
        val result = teamsRepository.getTeamsByLeagueName(leagueName)
        if (result.status == RequestStatus.SUCCESS) {
            result.data?.let {
                teamsRepository.saveTeams(it)
            }
        }
        return result
    }
}