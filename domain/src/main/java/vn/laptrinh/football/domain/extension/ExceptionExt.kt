package vn.laptrinh.football.domain.extension

import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus
import java.io.IOException

fun Exception.toNetworkResponse() = when (this) {
    is IOException -> NetworkResponse<Any>(null, RequestStatus.ERR_NETWORK)
    else -> NetworkResponse<Any>(null, RequestStatus.ERR_NOT_REACHED)
}