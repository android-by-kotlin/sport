package vn.laptrinh.football.domain.model

data class League(
    val id: Int,
    val name: String,
    val sportName: String?,
    val alternateName: String?
)