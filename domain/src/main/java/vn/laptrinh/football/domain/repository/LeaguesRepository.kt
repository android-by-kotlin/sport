package vn.laptrinh.football.domain.repository

import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse

interface LeaguesRepository {

    suspend fun getLeagues(): NetworkResponse<Leagues>
}