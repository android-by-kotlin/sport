package vn.laptrinh.football.domain.usecase

import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.repository.LeaguesRepository
import javax.inject.Inject

class GetLeaguesUseCase @Inject constructor(
    private val leaguesRepository: LeaguesRepository
) {

    suspend operator fun invoke(): NetworkResponse<Leagues> {
        return leaguesRepository.getLeagues()
    }
}