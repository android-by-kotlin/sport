package vn.laptrinh.football.domain.repository

import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams
import vn.laptrinh.football.domain.model.NetworkResponse
import kotlinx.coroutines.flow.Flow

interface TeamsRepository {

    suspend fun getTeamByName(name: String): Flow<Team?>

    suspend fun saveTeams(teams: Teams)

    suspend fun getTeamsByLeagueName(leagueName: String): NetworkResponse<Teams>
}