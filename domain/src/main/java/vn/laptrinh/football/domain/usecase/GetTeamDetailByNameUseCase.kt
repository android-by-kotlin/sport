package vn.laptrinh.football.domain.usecase

import vn.laptrinh.football.domain.repository.TeamsRepository
import javax.inject.Inject

class GetTeamDetailByNameUseCase @Inject constructor(
    private val teamsRepository: TeamsRepository
) {

    suspend operator fun invoke(
        name: String
    ) = teamsRepository.getTeamByName(name = name)
}