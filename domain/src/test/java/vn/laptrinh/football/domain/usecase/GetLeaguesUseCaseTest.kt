package vn.laptrinh.football.domain.usecase

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.repository.LeaguesRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class GetLeaguesUseCaseTest {

    @InjectMocks
    private lateinit var usecase: GetLeaguesUseCase

    @Mock
    private lateinit var leaguesRepository: LeaguesRepository

    @Test
    fun `WHEN getLeagues of LeaguesRepository gives a result THEN getLeagues returns this result`() = runTest {
        // GIVEN
        val expected = NetworkResponse<Leagues>()
        given(leaguesRepository.getLeagues()).willReturn(expected)

        // WHEN
        val actual = usecase()

        // THEN
        then(leaguesRepository).should().getLeagues()
        assertEquals(expected, actual)
    }
}