package vn.laptrinh.football.domain.usecase

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams
import vn.laptrinh.football.domain.repository.TeamsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class GetTeamsByLeagueNameUseCaseTest {

    @InjectMocks
    private lateinit var useCase: GetTeamsByLeagueNameUseCase

    @Mock
    private lateinit var teamsRepository: TeamsRepository

    @Test
    fun `WHEN getTeamsByLeagueName of TeamsRepository gives a error result THEN invoke of usecase returns this result and does not save to DB`() = runTest {
        // GIVEN
        val expected = NetworkResponse<Teams>()
        val leagueName = "Lingue 1"
        given(teamsRepository.getTeamsByLeagueName(leagueName)).willReturn(expected)

        // WHEN
        val actual = useCase(leagueName)

        // THEN
        then(teamsRepository).should().getTeamsByLeagueName(leagueName)
        then(teamsRepository).shouldHaveNoMoreInteractions()
        assertEquals(expected, actual)
    }

    @Test
    fun `WHEN getTeamsByLeagueName of TeamsRepository gives a success empty result THEN invoke of usecase returns this result and does not save to DB`() = runTest {
        // GIVEN
        val expected = NetworkResponse<Teams>(
            status = RequestStatus.SUCCESS
        )
        val leagueName = "Lingue 1"
        given(teamsRepository.getTeamsByLeagueName(leagueName)).willReturn(expected)

        // WHEN
        val actual = useCase(leagueName)

        // THEN
        then(teamsRepository).should().getTeamsByLeagueName(leagueName)
        then(teamsRepository).shouldHaveNoMoreInteractions()
        assertEquals(expected, actual)
    }

    @Test
    fun `WHEN getTeamsByLeagueName of TeamsRepository gives a success result THEN invoke of usecase returns this result and save to DB`() = runTest {
        // GIVEN
        val expected = NetworkResponse<Teams>(
            status = RequestStatus.SUCCESS,
            data = teams
        )
        val leagueName = "Lingue 1"
        given(teamsRepository.getTeamsByLeagueName(leagueName)).willReturn(expected)

        // WHEN
        val actual = useCase(leagueName)

        // THEN
        then(teamsRepository).should().getTeamsByLeagueName(leagueName)
        then(teamsRepository).should().saveTeams(teams)
        assertEquals(expected, actual)
    }

    companion object {
        private val team = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val teams = Teams(
            teams = listOf(team)
        )
    }
}