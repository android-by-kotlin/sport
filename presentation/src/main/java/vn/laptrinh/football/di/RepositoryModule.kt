package vn.laptrinh.football.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.football.data.repository.LeaguesRepositoryImpl
import vn.laptrinh.football.data.repository.TeamsRepositoryImpl
import vn.laptrinh.football.domain.repository.LeaguesRepository
import vn.laptrinh.football.domain.repository.TeamsRepository

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideTeamsRepository(
        repository: TeamsRepositoryImpl
    ): TeamsRepository

    @Binds
    abstract fun provideLeaguesRepository(
        repository: LeaguesRepositoryImpl
    ): LeaguesRepository
}
