package vn.laptrinh.football.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class TeamDetailUIModel(
    val name: TextUIModel,
    val bannerUrl: String,
    val countryName: TextUIModel,
    val leagueName: TextUIModel,
    val description: TextUIModel
)
