package vn.laptrinh.football.presentation.screen

enum class ScreenType {
    TEAMS,
    TEAM_DETAIL
}