package vn.laptrinh.football.presentation.extension

import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus

fun NetworkResponse<Leagues>.toLeaguesNames() = when (status) {
    RequestStatus.SUCCESS -> data?.leagues.orEmpty().sortedBy {
        it.name
    }.map {
        it.name
    }
    else -> emptyList()
}
