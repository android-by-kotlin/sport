package vn.laptrinh.football.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class TeamUIModel(
    val badgeUrl : String,
    val name : String
)
