package vn.laptrinh.football.presentation.ui.model.previewprovider

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import vn.laptrinh.football.presentation.ui.model.TeamUIModel

class TeamUIModelPreviewProvider : PreviewParameterProvider<TeamUIModel> {

    override val values: Sequence<TeamUIModel> = sequenceOf(
        TeamUIModel(
            badgeUrl = "https://www.thesportsdb.com/images/media/team/badge/wq9sir1639406443.png",
            name = "Barcelona"
        )
    )
}