package vn.laptrinh.football.presentation.component

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import vn.laptrinh.football.presentation.R
import vn.laptrinh.football.presentation.annotation.CombinedPreviews
import vn.laptrinh.football.presentation.ui.model.TeamUIModel
import vn.laptrinh.football.presentation.ui.model.previewprovider.TeamUIModelPreviewProvider

@Composable
fun TeamComponent(
    uiModel: TeamUIModel,
    onClick: () -> Unit
) {
    Card(
        modifier = Modifier
            .wrapContentSize()
            .padding(vertical = 8.dp),
        shape = RoundedCornerShape(84.dp),
        onClick = onClick
    ) {
        AsyncImage(
            modifier = Modifier.size(168.dp),
            model = uiModel.badgeUrl,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            error = painterResource(id = R.drawable.ic_broken_image),
            placeholder = painterResource(id = R.drawable.ic_loading_image)
        )
    }
}

@Composable
@CombinedPreviews
fun PreviewTeamComponent(
    @PreviewParameter(TeamUIModelPreviewProvider::class)
    uiModel: TeamUIModel
) {
    TeamComponent(
        uiModel = uiModel,
        onClick = {}
    )
}