package vn.laptrinh.football.presentation.extension

import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus
import vn.laptrinh.football.domain.model.Teams
import vn.laptrinh.football.presentation.ui.model.TeamUIModel

fun NetworkResponse<Teams>.toTeamUIModels() = when (status) {
    RequestStatus.SUCCESS -> data?.teams.orEmpty().sortedBy { team ->
        team.name
    }.map { team ->
        TeamUIModel(
            badgeUrl = team.badgeUrl.orEmpty(),
            name = team.name
        )
    }
    else -> emptyList()
}
