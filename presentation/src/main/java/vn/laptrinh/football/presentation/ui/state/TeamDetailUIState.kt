package vn.laptrinh.football.presentation.ui.state

import androidx.compose.runtime.Immutable
import vn.laptrinh.football.presentation.ui.model.ErrorUIModel
import vn.laptrinh.football.presentation.ui.model.TeamDetailUIModel

@Immutable
sealed class TeamDetailUIState {
    @Immutable
    object Loading : TeamDetailUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : TeamDetailUIState()

    @Immutable
    data class Success(
        val uiModel: TeamDetailUIModel
    ) : TeamDetailUIState()
}