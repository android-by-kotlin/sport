package vn.laptrinh.football.presentation.component

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import vn.laptrinh.football.presentation.ui.model.TeamUIModel

@Composable
fun TeamsComponent(
    uiModel: List<TeamUIModel>,
    onTeamClick: (String) -> Unit
) {
    val numberColumns = 2
    LazyVerticalGrid(
        modifier = Modifier.fillMaxWidth(),
        columns = GridCells.Fixed(numberColumns)
    ) {
        items(numberColumns) {
            Spacer(modifier = Modifier.height(8.dp))
        }
        uiModel.forEach { teamUIModel ->
            item {
                TeamComponent(
                    uiModel = teamUIModel,
                    onClick = { onTeamClick(teamUIModel.name) }
                )
            }
        }
        items(numberColumns) {
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}