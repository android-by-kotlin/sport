package vn.laptrinh.football.presentation.ui.state

import androidx.compose.runtime.Immutable
import vn.laptrinh.football.presentation.ui.model.ErrorUIModel
import vn.laptrinh.football.presentation.ui.model.TeamUIModel

@Immutable
sealed class FootballUIState {
    @Immutable
    data object Loading : FootballUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : FootballUIState()

    @Immutable
    data class Success(
        val leagues: List<String>,
        val teams: List<TeamUIModel>
    ) : FootballUIState()
}