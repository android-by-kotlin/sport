package vn.laptrinh.football.presentation.screen

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import vn.laptrinh.football.presentation.FootballViewModel
import vn.laptrinh.football.presentation.component.ErrorComponent
import vn.laptrinh.football.presentation.component.LoaderComponent
import vn.laptrinh.football.presentation.component.TeamDetailComponent
import vn.laptrinh.football.presentation.ui.state.TeamDetailUIState

@Composable
fun TeamDetailScreen(
    viewModel: FootballViewModel,
    navController: NavHostController,
) {
    BackHandler {
        navController.navigate(ScreenType.TEAMS.name)
    }
    when (
        val uiState = viewModel.teamDetailState.collectAsStateWithLifecycle(
            lifecycleOwner = LocalLifecycleOwner.current
        ).value
    ) {
        TeamDetailUIState.Loading -> LoaderComponent()
        is TeamDetailUIState.Error -> ErrorComponent(
            uiModel = uiState.uiModel,
            onButtonClick = { viewModel.initTeamDetailState() }
        )
        is TeamDetailUIState.Success -> TeamDetailComponent(uiModel = uiState.uiModel)
    }
}