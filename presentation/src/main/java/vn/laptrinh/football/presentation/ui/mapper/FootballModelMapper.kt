package vn.laptrinh.football.presentation.ui.mapper

import vn.laptrinh.football.domain.usecase.GetTeamsByLeagueNameUseCase
import vn.laptrinh.football.presentation.R
import vn.laptrinh.football.presentation.extension.toTeamUIModels
import vn.laptrinh.football.presentation.ui.model.ErrorUIModel
import vn.laptrinh.football.presentation.ui.model.TextUIModel
import vn.laptrinh.football.presentation.ui.state.FootballUIState
import javax.inject.Inject

class FootballModelMapper @Inject constructor(
    private val getTeamsByLeagueNameUseCase: GetTeamsByLeagueNameUseCase
) {

    fun toSuccessUIState(
        allLeagues: List<String>
    ) = FootballUIState.Success(
        leagues = allLeagues,
        teams = emptyList()
    )

    fun toErrorUIState() = FootballUIState.Error(
        uiModel = ErrorUIModel(
            text = TextUIModel.StringResource(R.string.error_of_get_leagues)
        )
    )

    suspend fun updateUIStateBySearchText(
        currentState: FootballUIState,
        searchText: String,
        allLeagues: List<String>
    ): FootballUIState {
        if (currentState !is FootballUIState.Success) return currentState
        val teamsResult = getTeamsByLeagueNameUseCase(searchText)
        val filteredLeagues = allLeagues.filter {
            searchText.isBlank() || it.contains(other = searchText.lowercase(), ignoreCase = true)
        }
        return currentState.copy(
            leagues = filteredLeagues,
            teams = teamsResult.toTeamUIModels()
        )
    }
}