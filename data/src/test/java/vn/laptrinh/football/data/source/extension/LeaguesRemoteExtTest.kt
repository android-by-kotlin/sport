package vn.laptrinh.football.data.source.extension

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import vn.laptrinh.football.data.source.remote.model.LeagueRemote
import vn.laptrinh.football.data.source.remote.model.LeaguesRemote
import vn.laptrinh.football.domain.model.League
import vn.laptrinh.football.domain.model.Leagues

class LeaguesRemoteExtTest {

    @Test
    fun toLeagues() {
        assertEquals(leagues, leaguesRemote.toLeagues())
    }

    companion object {

        private val premierLeague = League(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val premierLeagueRemote = LeagueRemote(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val leagues = Leagues(
            leagues = listOf(premierLeague)
        )
        private val leaguesRemote = LeaguesRemote(
            leagues = listOf(premierLeagueRemote)
        )
    }
}