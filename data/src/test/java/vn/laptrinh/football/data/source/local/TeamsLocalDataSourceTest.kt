package vn.laptrinh.football.data.source.local

import vn.laptrinh.football.data.source.local.db.dao.TeamDao
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class TeamsLocalDataSourceTest {

    private lateinit var dataSource: TeamsLocalDataSource

    @MockK
    private lateinit var teamDao: TeamDao

    @BeforeEach
    fun setUp() {
        dataSource = TeamsLocalDataSource(
            dispatcher = Dispatchers.Unconfined,
            teamDao = teamDao
        )
    }

    @ParameterizedTest(name = "WHEN Teams: {0} THEN saveTeams saves: {1} to db")
    @MethodSource("provideTestDataForSaveTeams")
    fun saveTeams(
        teams: Teams,
        teamsEntities: List<TeamEntity>
    ) = runTest {
        // GIVEN
        every { teamDao.saveAll(teamsEntities) } returns Unit

        // WHEN
        dataSource.saveTeams(teams)

        // THEN
        verify { teamDao.saveAll(teamsEntities) }
    }

    @ParameterizedTest(name = "WHEN team name: {0}, findByName of teamDao gives: {1} THEN findTeamByName returns: {2}")
    @MethodSource("provideTestDataForFindByName")
    fun findTeamByName(
        teamName: String,
        entityFlow: Flow<TeamEntity?>,
        expected: Flow<Team>
    ) = runTest {
        // GIVEN
        every { teamDao.findByName(teamName) } returns entityFlow

        // WHEN
        val actual = dataSource.findTeamByName(teamName)

        // THEN
        verify { teamDao.findByName(teamName) }
        assertEquals(expected.first(), actual.first())
    }

    companion object {

        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val psgTeamEntity = TeamEntity(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private const val TEAM_NAME = "PSG"

        @JvmStatic
        private fun provideTestDataForSaveTeams(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    Teams(teams = null),
                    emptyList<TeamEntity>()
                ),
                Arguments.of(
                    Teams(teams = emptyList()),
                    emptyList<TeamEntity>()
                ),
                Arguments.of(
                    Teams(teams = listOf(psgTeam)),
                    listOf(psgTeamEntity)
                )
            )
        }

        @JvmStatic
        private fun provideTestDataForFindByName(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    TEAM_NAME,
                    flowOf<TeamEntity?>(null),
                    flowOf<Team?>(null)
                ),
                Arguments.of(
                    TEAM_NAME,
                    flowOf<TeamEntity?>(psgTeamEntity),
                    flowOf<Team?>(psgTeam)
                )
            )
        }
    }
}