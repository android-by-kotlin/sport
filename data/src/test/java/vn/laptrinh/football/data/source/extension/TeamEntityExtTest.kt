package vn.laptrinh.football.data.source.extension

import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import vn.laptrinh.football.domain.model.Team
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TeamEntityExtTest {

    @Test
    fun toTeam() {
        assertEquals(psgTeam, psgTeamEntity.toTeam())
    }

    companion object {

        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val psgTeamEntity = TeamEntity(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
    }
}