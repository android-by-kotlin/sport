package vn.laptrinh.football.data.repository

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import vn.laptrinh.football.data.source.remote.LeaguesRemoteDataSource
import vn.laptrinh.football.data.source.remote.model.LeagueRemote
import vn.laptrinh.football.data.source.remote.model.LeaguesRemote
import vn.laptrinh.football.domain.model.League
import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class LeaguesRepositoryImplTest {

    @InjectMocks
    private lateinit var repository: LeaguesRepositoryImpl

    @Mock
    private lateinit var leaguesRemoteDataSource: LeaguesRemoteDataSource

    @Test
    fun `WHEN getLeagues of LeaguesRemoteDataSource gives the leagues remote THEN getLeagues returns these matching leagues`() = runTest {
        // GIVEN
        val leaguesRemoteNetworkResponse = NetworkResponse(
            data = LeaguesRemote(leagues = listOf(premierLeagueRemote))
        )
        given(leaguesRemoteDataSource.getLeagues()).willReturn(leaguesRemoteNetworkResponse)

        // WHEN
        val actual = repository.getLeagues()

        // THEN
        then(leaguesRemoteDataSource).should().getLeagues()
        val expected = NetworkResponse(
            data = Leagues(leagues = listOf(premierLeague))
        )
        assertEquals(expected, actual)
    }

    companion object {

        private val premierLeague = League(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val premierLeagueRemote = LeagueRemote(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
    }
}