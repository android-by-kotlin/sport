package vn.laptrinh.football.data.source.remote

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import vn.laptrinh.football.data.source.remote.model.LeaguesRemote
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.RequestStatus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import retrofit2.Call
import retrofit2.Response
import java.net.HttpURLConnection

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class LeaguesRemoteDataSourceTest {

    @InjectMocks
    private lateinit var dataSource: LeaguesRemoteDataSource

    @Mock
    private lateinit var ws: LeaguesWS

    @Mock
    private lateinit var call: Call<LeaguesRemote>

    @Test
    fun `WHEN LeaguesWS gives a result THEN getLeagues returns this result`() = runTest {
        // GIVEN
        val leagues = LeaguesRemote(leagues = emptyList())
        given(ws.getRemoteLeagues()).willReturn(call)
        given(call.execute()).willReturn(Response.success(leagues))

        // WHEN
        val actual = dataSource.getLeagues()

        // THEN
        then(ws).should().getRemoteLeagues()
        then(call).should().execute()
        val expected = NetworkResponse(
            data = leagues,
            status = RequestStatus.SUCCESS,
            code = HttpURLConnection.HTTP_OK
        )
        assertEquals(expected, actual)
    }
}