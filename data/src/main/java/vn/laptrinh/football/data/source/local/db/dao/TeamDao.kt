package vn.laptrinh.football.data.source.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface TeamDao {

    @Query("SELECT * FROM TeamEntity WHERE name = :name LIMIT 1")
    fun findByName(name: String): Flow<TeamEntity?>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(teams: List<TeamEntity>)
}