package vn.laptrinh.football.data.source.local

import vn.laptrinh.football.data.source.local.db.dao.TeamDao
import vn.laptrinh.football.data.source.extension.toEntity
import vn.laptrinh.football.data.source.extension.toTeam
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TeamsLocalDataSource @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val teamDao: TeamDao
) {

    suspend fun findTeamByName(
        name: String
    ): Flow<Team?> {
        return withContext(dispatcher) {
            teamDao.findByName(name).map {
                it?.toTeam()
            }
        }
    }

    suspend fun saveTeams(teams: Teams) {
        withContext(dispatcher) {
            val teamsEntities = teams.teams.orEmpty().map {
                it.toEntity()
            }
            teamDao.saveAll(
                teamsEntities
            )
        }
    }
}