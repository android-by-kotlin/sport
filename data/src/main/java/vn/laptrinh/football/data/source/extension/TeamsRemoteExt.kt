package vn.laptrinh.football.data.source.extension

import vn.laptrinh.football.data.source.remote.model.TeamsRemote
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams

fun TeamsRemote.toTeams() = Teams(
    teams = teams?.map {
        Team(
            id = it.id,
            name = it.name,
            logoUrl = it.logoUrl,
            badgeUrl = it.badgeUrl,
            bannerUrl = it.bannerUrl,
            countryName = it.countryName,
            leagueName = it.leagueName,
            englishDescription = it.englishDescription
        )
    }
)