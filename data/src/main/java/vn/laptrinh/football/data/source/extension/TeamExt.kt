package vn.laptrinh.football.data.source.extension

import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import vn.laptrinh.football.domain.model.Team

fun Team.toEntity() = TeamEntity(
    id = id,
    name = name,
    logoUrl = logoUrl,
    badgeUrl = badgeUrl,
    bannerUrl = bannerUrl,
    countryName = countryName,
    leagueName = leagueName,
    englishDescription = englishDescription
)