package vn.laptrinh.football.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class TeamRemote(
    @SerializedName("idTeam") val id: String,
    @SerializedName("strTeam") val name: String,
    @SerializedName("strLogo") val logoUrl: String?,
    @SerializedName("strBadge") val badgeUrl: String?,
    @SerializedName("strBanner") val bannerUrl: String?,
    @SerializedName("strCountry") val countryName: String?,
    @SerializedName("strLeague") val leagueName: String,
    @SerializedName("strDescriptionEN") val englishDescription: String?
)
