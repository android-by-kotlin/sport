package vn.laptrinh.football.data.source.remote.model

data class TeamsRemote(
    val teams: List<TeamRemote>? = emptyList()
)