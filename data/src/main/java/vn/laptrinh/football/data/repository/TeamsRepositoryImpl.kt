package vn.laptrinh.football.data.repository

import vn.laptrinh.football.data.source.extension.toTeams
import vn.laptrinh.football.data.source.local.TeamsLocalDataSource
import vn.laptrinh.football.data.source.remote.TeamsRemoteDataSource
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.model.Teams
import vn.laptrinh.football.domain.repository.TeamsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TeamsRepositoryImpl @Inject constructor(
    private val teamsLocalDataSource: TeamsLocalDataSource,
    private val teamsRemoteDataSource: TeamsRemoteDataSource
) : TeamsRepository {

    override suspend fun getTeamByName(name: String): Flow<Team?> {
        return teamsLocalDataSource.findTeamByName(name)
    }

    override suspend fun saveTeams(teams: Teams) {
        teamsLocalDataSource.saveTeams(teams)
    }

    override suspend fun getTeamsByLeagueName(leagueName: String): NetworkResponse<Teams> {
        return teamsRemoteDataSource.getTeamsByLeagueName(leagueName).run {
            NetworkResponse(
                data = data?.toTeams(),
                status = status,
                code = code
            )
        }
    }
}