package vn.laptrinh.football.data.source.remote.model

data class LeaguesRemote(
    val leagues: List<LeagueRemote> = emptyList()
)