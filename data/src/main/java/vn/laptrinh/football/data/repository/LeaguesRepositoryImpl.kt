package vn.laptrinh.football.data.repository

import vn.laptrinh.football.data.source.extension.toLeagues
import vn.laptrinh.football.data.source.remote.LeaguesRemoteDataSource
import vn.laptrinh.football.domain.model.Leagues
import vn.laptrinh.football.domain.model.NetworkResponse
import vn.laptrinh.football.domain.repository.LeaguesRepository
import javax.inject.Inject

class LeaguesRepositoryImpl @Inject constructor(
    private val leaguesRemoteDataSource: LeaguesRemoteDataSource
) : LeaguesRepository {

    override suspend fun getLeagues(): NetworkResponse<Leagues> {
        return leaguesRemoteDataSource.getLeagues().run {
            NetworkResponse(
                data = data?.toLeagues(),
                status = status,
                code = code
            )
        }
    }
}