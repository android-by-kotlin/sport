package vn.laptrinh.football.data.source.extension

import vn.laptrinh.football.data.source.remote.model.LeaguesRemote
import vn.laptrinh.football.domain.model.League
import vn.laptrinh.football.domain.model.Leagues

fun LeaguesRemote.toLeagues() = Leagues(
    leagues = leagues.map {
        League(
            id = it.id,
            name = it.name,
            sportName = it.sportName,
            alternateName = it.alternateName
        )
    }
)