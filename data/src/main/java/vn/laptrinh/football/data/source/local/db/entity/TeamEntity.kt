package vn.laptrinh.football.data.source.local.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TeamEntity(
    @PrimaryKey val id: String,
    val leagueName: String,
    val name: String,
    val logoUrl: String?,
    val badgeUrl: String?,
    val bannerUrl: String?,
    val countryName: String?,
    val englishDescription: String?
)