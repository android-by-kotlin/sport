package vn.laptrinh.football.data.source.remote

import vn.laptrinh.football.data.source.remote.model.LeaguesRemote
import vn.laptrinh.football.domain.model.NetworkResponse
import javax.inject.Inject

class LeaguesRemoteDataSource @Inject constructor(
    private val ws: LeaguesWS
) : ApiDataSource<LeaguesRemote>() {

    suspend fun getLeagues(): NetworkResponse<LeaguesRemote> = launchRequest(ws.getRemoteLeagues())
}

