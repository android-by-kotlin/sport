package vn.laptrinh.football.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class LeagueRemote(
    @SerializedName("idLeague") val id: Int,
    @SerializedName("strLeague") val name: String,
    @SerializedName("strSport") val sportName: String?,
    @SerializedName("strLeagueAlternate") val alternateName: String?
)